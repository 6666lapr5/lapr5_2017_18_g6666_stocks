﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GestaoDeFornecedores.Models;
using GestaoDeFornecedores.DTOs;
using System.IO;
using System.Net;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using System.Collections.Specialized;
using System;

namespace GestaoDeFornecedores.Controllers
{

    [Produces("application/json")]
    [Route("api/Encomendas")]
    public class EncomendasController : Controller
    {
        private readonly EncomendasContext _context;
        
        public EncomendasController(EncomendasContext context)
        {
            _context = context;
        }
        /*
        // GET: api/Encomendas
        [HttpGet]
        public IEnumerable<Encomenda> GetEncomenda()
        {
            return _context.Encomenda;
        }

        // GET: api/Encomendas/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEncomenda([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var encomenda = await _context.Encomenda.SingleOrDefaultAsync(m => m.Id == id);

            if (encomenda == null)
            {
                return NotFound();
            }

            return Ok(encomenda);
        }

        // PUT: api/Encomendas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEncomenda([FromRoute] int id, [FromBody] Encomenda encomenda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != encomenda.Id)
            {
                return BadRequest();
            }

            _context.Entry(encomenda).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EncomendaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        */
        // POST: api/Encomendas
        [HttpPost]
        public async Task<IActionResult> PostEncomenda([FromBody] EncomendaPostDTO encomendado)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Encomenda encomenda = new Encomenda()
            {
                NomeFarmacia = encomendado.Farmacia,
                ApresentacaoId = encomendado.ApresentacaoId,
                Preferencia = encomendado.Preferencia,
                Entregue = false,
            };

            _context.Encomenda.Add(encomenda);
            await _context.SaveChangesAsync();

            /**
            Farmacia farmacia = new Farmacia()
            {
                nome = encomendado.Farmacia,
                Longitude = encomendado.Longitude,
                Latitude = encomendado.Latitude,
                Manha = encomendado.Manha,
            };

            _context.Farmacia.Add(farmacia);
            await _context.SaveChangesAsync();

            **/
            
            //return CreatedAtAction("GetEncomenda", new { id = encomenda.Id }, encomenda);
            return Ok(encomenda);
        }



        /**
        [HttpPut("/restock")]
        public async Task<IActionResult> PutEntrega([FromBody] EntregaDTO entrega)
        {

            var request = (HttpWebRequest)WebRequest.Create("http://localhost:8443/api/stock/restock");
            request.Method = "PUT";
            request.ContentType = "application/xml";
            if (entrega != null)  {
                request.ContentLength = Size(payload);
                Stream dataStream = request.GetRequestStream();
                Serialize(dataStream, payload);
                dataStream.Close();
            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string returnString = response.StatusCode.ToString();
        }

        public void Serialize(Stream output, object input)
        {
            var ser = new DataContractSerializer(input.GetType());
            ser.WriteObject(output, input);
        }

**/

        /**
               // DELETE: api/Encomendas/5
               [HttpDelete("{id}")]
               public async Task<IActionResult> DeleteEncomenda([FromRoute] int id)
               {
                   if (!ModelState.IsValid)
                   {
                       return BadRequest(ModelState);
                   }

                   var encomenda = await _context.Encomenda.SingleOrDefaultAsync(m => m.Id == id);
                   if (encomenda == null)
                   {
                       return NotFound();
                   }

                   _context.Encomenda.Remove(encomenda);
                   await _context.SaveChangesAsync();

                   return Ok(encomenda);
               }
        **/

        /**    
        private bool EncomendaExists(int id)
        {
            return _context.Encomenda.Any(e => e.Id == id);
        }

        public void PercorreEncomendas()
        {
            string path = @"../../../lapr5_2017_18_g6666_prolog/base.pl";
            string rFile = @"../../../lapr5_2017_18_g6666_prolog/path";
            string runPL = "http://localhost:8000/run";
            string line;
            char[] delimiterChars = { ',', '[', ']' };
            List<Farmacia> farmM = new List<Farmacia>();
            List<Farmacia> farmT = new List<Farmacia>();

            // ler farmacias da DB

            var farmacias = from f in _context.Farmacia
                               select new Farmacia()
                               {
                                   nome = f.nome,
                                   Latitude = f.Latitude,
                                   Longitude = f.Longitude,
                                   Manha = f.Manha
                               };

            foreach(Farmacia f in farmacias)
            {
                if (f.Manha == true)
                    farmM.Add(f);
                else
                    farmT.Add(f);
            }
            // criar ficheiro com base de conhecimento
            StreamWriter sw = new StreamWriter(path);
            StreamReader sr = new StreamReader(rFile);
            //fornecedor
            Farmacia begin = new Farmacia
            {
                nome = "fornecedor",
                Latitude = 41.1931965,
                Longitude = -8.6104970,
                Manha = true
            };
            if (farmM.Count != 0)
            {
                foreach (Farmacia f in farmM)
                {
                    // cria base de conhecimento
                    line = "(" + f.nome + "," + f.Latitude + "," + f.Longitude + "," + f.Manha + ").\n";
                    sw.Write(line);
                }
                line = "(" + begin.nome + "," + begin.Latitude + "," + begin.Longitude + "," + begin.Manha + ").\n";
                sw.Write(line);
                WebRequest req = WebRequest.Create(runPL);
 
                req.Method = "POST";
                req.Timeout = 100000;
                req.ContentType = "application/x-www-form-urlencoded";
                byte[] sentData = Encoding.UTF8.GetBytes("tsp2(fornecedor,Cam,X)");
                req.ContentLength = sentData.Length;
                using (System.IO.Stream sendStream = req.GetRequestStream())
                {
                    sendStream.Write(sentData, 0, sentData.Length);
                    sendStream.Close();
                }

                
            }
            string read = sr.ReadLine();
            string[] fast = read.Split(delimiterChars);
            // apaga data do ficheiro
            System.IO.File.WriteAllText(rFile, string.Empty);

            if (farmT.Count != 0)
            {
                foreach (Farmacia f in farmT)
                {
                    // cria base de conhecimento
                    line = "(" + f.nome + "," + f.Latitude + "," + f.Longitude + "," + f.Manha + ").\n";
                    sw.Write(line);
                }
                Boolean send = false;
                foreach(Farmacia x in farmM)
                {
                    if(x.nome == fast.Last().ToString())
                    {
                        line = ("(" + x.nome + ","+x.Latitude+","+x.Longitude+","+x.Manha+").\n");
                        
                        sw.Write(line);
                        WebRequest request = WebRequest.Create(runPL);
                        request.Method = "POST";
                        request.Timeout = 100000;
                        request.ContentType = "application/x-www-form-urlencoded";
                        byte[] sentData = Encoding.UTF8.GetBytes("tsp2(" + x.nome + ",Cam,X)");
                        request.ContentLength = sentData.Length;
                        using (System.IO.Stream sendStream = request.GetRequestStream())
                        {
                            sendStream.Write(sentData, 0, sentData.Length);
                            sendStream.Close();
                        }
                        send = true;
                    }
                }
                if( send == false)
                {
                    WebRequest req = WebRequest.Create(runPL);
                    req.Method = "POST";
                    req.Timeout = 100000;
                    req.ContentType = "application/x-www-form-urlencoded";
                    byte[] sentData = Encoding.UTF8.GetBytes("tsp2(fornecedor,Cam,X)");
                    req.ContentLength = sentData.Length;
                    using (System.IO.Stream sendStream = req.GetRequestStream())
                    {
                        sendStream.Write(sentData, 0, sentData.Length);
                        sendStream.Close();
                    }

                }
                
    
            }
            read = sr.ReadLine();
            string[] fast2 = read.Split(delimiterChars);
            System.IO.File.WriteAllText(rFile, string.Empty);



        }
        **/
    }
}