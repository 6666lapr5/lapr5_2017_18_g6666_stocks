﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GestaoDeFornecedores.Migrations
{
    public partial class _5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LinhaEncomenda");

            migrationBuilder.DropColumn(
                name: "Farmacia",
                table: "Encomenda");

            migrationBuilder.DropColumn(
                name: "Preferencia",
                table: "Encomenda");

            migrationBuilder.AddColumn<string>(
                name: "NomeFarmacia",
                table: "Encomenda",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NomeFarmacia",
                table: "Encomenda");

            migrationBuilder.AddColumn<string>(
                name: "Farmacia",
                table: "Encomenda",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Preferencia",
                table: "Encomenda",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LinhaEncomenda",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Apresentacao = table.Column<string>(nullable: true),
                    ApresentacaoId = table.Column<string>(nullable: true),
                    EncomendaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LinhaEncomenda", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LinhaEncomenda_Encomenda_EncomendaId",
                        column: x => x.EncomendaId,
                        principalTable: "Encomenda",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LinhaEncomenda_EncomendaId",
                table: "LinhaEncomenda",
                column: "EncomendaId");
        }
    }
}
