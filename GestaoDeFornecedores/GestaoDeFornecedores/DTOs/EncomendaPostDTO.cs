﻿using GestaoDeFornecedores.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestaoDeFornecedores.DTOs
{
    public class EncomendaPostDTO
    {
        public string Farmacia { get; set; }
       // public Double Latitude { get; set; }
       // public Double Longitude { get; set; }
        // if manha = true, schedule is in the morning, else afternoon
        //public Boolean Manha { get; set; }
        public string ApresentacaoId { get; set; }
        public string Preferencia { get; set; }
        public Boolean Entregue { get; set; }


    }
}
