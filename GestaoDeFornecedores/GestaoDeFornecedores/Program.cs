﻿using GestaoDeFornecedores.Controllers;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Reactive.Linq;

namespace GestaoDeFornecedores
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DateTime now = DateTime.Now;
            DateTime every7h = new DateTime(now.Year, now.Month, now.Day, 7, 0, 0, 0);
            // create variable that will observe method.
            // Starts today at 7am and then every 24h will execute method PercorreEncomendas.
            var source = Observable.Timer(every7h, TimeSpan.FromHours(24)).Timestamp() ;
            //EncomendasController obj = new EncomendasController();


            //source.Subscribe(x => obj.PercorreEncomendas());
            //obj.PercorreEncomendas();
            BuildWebHost(args).Run();
        }

     

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
