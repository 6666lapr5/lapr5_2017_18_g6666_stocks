﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using GestaoDeFornecedores.Models;

namespace GestaoDeFornecedores.Models
{
    public class EncomendasContext : DbContext
    {
        public EncomendasContext (DbContextOptions<EncomendasContext> options)
            : base(options)
        {
        }

        public DbSet<GestaoDeFornecedores.Models.Encomenda> Encomenda { get; set; }

        public DbSet<GestaoDeFornecedores.Models.Farmacia> Farmacia { get; set; }
    }
}
