﻿using GestaoDeFornecedores.DTOs;
using System;

namespace GestaoDeFornecedores.Models
{
    public class Encomenda
    {
        internal Farmacia f;

        public int Id { get; set; }
        public string NomeFarmacia { get; set; }
        public string ApresentacaoId { get; set; }
        public Boolean Entregue { get; set; }
        public string Preferencia { get; set; }


    }
}
