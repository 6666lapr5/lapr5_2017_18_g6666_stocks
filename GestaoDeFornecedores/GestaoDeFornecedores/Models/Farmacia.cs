﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestaoDeFornecedores.Models
{
    public class Farmacia
    {
        public int Id { get; set; }
        public string nome { get; set; }
        public Double Latitude { get; set; }
        public Double Longitude { get; set; }
        public Boolean Manha { get; set; }
    }
}
